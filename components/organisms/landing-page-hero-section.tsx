import Link from "next/link";
import LandingPageHeroCarousel from "../molecules/landing-page-hero-carousel";
import { Button } from "../ui/button";
export default function LandingPageHeroSection() {
  return (
    <section className="relative grid w-full justify-center">
      <section className="mt-8 grid w-full max-w-screen-xl items-start gap-8 md:grid-cols-2 md:gap-10">
        <section className="mt-0 grid gap-4 p-10 sm:gap-8 md:mt-10">
          <h1 className="grid font-bold">
            <span className="text-xl sm:text-2xl">Welcome to </span>
            <span className="whitespace-nowrap text-3xl text-orange-400 md:text-5xl">
              G-Connect! 🔗
            </span>
          </h1>
          <p className="text-md max-w-md">
            We&apos;re here to help you find someone to study with, whether
            you&apos;re looking for someone to help you with a specific course
            or just someone to motivate you to stay on track.
          </p>
          <Button variant="purple" className="justify-self-start" asChild>
            <Link href="?">Get Started</Link>
          </Button>
        </section>
        <LandingPageHeroCarousel />
      </section>

      <section className="absolute bottom-0 -z-50 hidden h-[100px] w-full bg-indigo-100 md:block" />
    </section>
  );
}
